var app = new Vue({
	el: '#work_area',
	data: {
	destino: '',
	numPasajeros:1,
	fechaPartida:'',
	fechaRetorno:'',
	arDestinos:[],
	arCotizacion:[],
	arCaracteristicas:[],
	arError:[],
	precio_standard:0,
	precio_premium:0,
	},
  	computed: {
  		listarDestinos(){
  			var arDestinos = [];
  			arDestinos.push({id:'0', text:'Seleccione'});
  			this.arDestinos.forEach(function(o,i){
  				var objeto = {id:o, text:o};
  				arDestinos.push(objeto);
  			});
  			return arDestinos;
  		},
  		contadorErrores(){
  			return this.arError.length > 0 ? true : false;
  		}
  	},	
	created:function(){
		this.getDestinos();
		this.mostrarDatePicker();
	},
	methods: {
		noEscribir: function(event){
			event.preventDefault();
			return false;
		},
		mostrarDatePicker: function(){
			setTimeout(
				function(){ 
					jQuery('.mydatepicker, #datepicker').datepicker({format: "dd/mm/yyyy",autoclose: true}).on('changeDate', function(e) {
        				validarFecha();
        			}); 
				}, 1000);
		},		
		formatearFecha: function(fecha){
			return moment(fecha).format('DD/MM/YYYY') == 'Invalid date' ? '' : moment(fecha).format('DD/MM/YYYY');
		},		
		getDestinos: function(){
            this.$http.get('https://testsoat.interseguro.com.pe/talentfestapi/destinos').then((response) => {
                this.arDestinos = response.data;


            }, (response) => {
                console.log('Error: no se pudo obtener la lista de destinos');
            }); 			
		},
		obtenerDatosCarac: function(index,indexDos){
			return  this.arCotizacion[index].caracteristicas[indexDos].aplica;
		},
		cotizar: function(){
			this.validarParams();
			if(this.arError.length == 0){
				fec_inicio = $('#fec_ini').val();
		    	fec_fin = $('#fec_fin').val();					
				var objeto = {
					destino: this.destino,
					fecha_partida: fec_inicio,
					fecha_retorno: fec_fin,
					cantidad_pasajeros: this.numPasajeros 
				};
				var url = 'https://testsoat.interseguro.com.pe/talentfestapi/cotizacion';
	            this.$http.post(url,objeto).then((response) => {
	                console.log(response.data);
	                this.arCotizacion = response.data;
	            	if(response.data.length == 2){
	            		this.precio_standard = response.data[0].precio_plan.toFixed(1);
	            		this.precio_premium = response.data[1].precio_plan.toFixed(1);
	            		this.arCaracteristicas = response.data[0].caracteristicas;
	            	}
	            }, (response) => {
	                console.log('Error: no se pudo obtener la cotización');
	            });					
			}
		},
		validarParams: function(){
			this.arError = [];
			if(this.destino == '0' || this.destino.length == 0){
				this.arError.push('Por favor elija un destino');
			}
			if(parseInt(this.numPasajeros) <= 0){
				this.arError.push('Mínimo debe ingresar un pasajero');
			}
			fec_inicio = $('#fec_ini').val();
	    	fec_fin = $('#fec_fin').val();			
			
			if(fec_inicio == '' || fec_inicio.length == 0){
				this.arError.push('Por favor elija una fecha de Partida');
			}
			if(fec_fin == '' || fec_fin.length == 0){
				this.arError.push('Por favor elija una fecha de Retorno');
			}									
		},
		isNumber: function(evt) {
			evt = (evt) ? evt : window.event;
			var charCode = (evt.which) ? evt.which : evt.keyCode;
			if ((charCode > 31 && (charCode < 48 || charCode > 57)) && charCode !== 46) {
				evt.preventDefault();
			} else {
				return true;
			}
		},
	}  
});

function validarFecha(){
	fec_inicio = moment($('#fec_ini').val(),'DD/MM/YYYY').format();
    fec_fin = moment($('#fec_fin').val(),'DD/MM/YYYY').format();

    if(fec_fin < fec_inicio || fec_inicio > fec_fin ) {
      alert('la fecha de partida debe ser menor a la fecha de retorno');
      $('#fec_fin').val("");
    }
}